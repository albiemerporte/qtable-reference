
from PyQt6 import uic
from PyQt6.QtWidgets import *
from sqlshot import sqlqueryselecttbl, sqlquerytitlesearch   #ignore this, this is to connect w/ sqlite3

class Main:
    def __init__(self):
        self.mainui = uic.loadUi('main.ui')
        self.mainui.editsearch.setFocus()
        self.mainui.editsearch.textChanged.connect(self.search)
        self.loaddata()
    
    def show(self):
        self.mainui.show()

    def loaddata(self):
        people = sqlqueryselecttbl()
        
        for person in people:
            inx = people.index(person)
            self.mainui.tbllibrary.insertRow(inx)
        
            self.mainui.tbllibrary.setItem(inx, 0, QTableWidgetItem(str(person[0])))
            self.mainui.tbllibrary.setItem(inx, 1, QTableWidgetItem(str(person[1])))
            self.mainui.tbllibrary.setItem(inx, 2, QTableWidgetItem(person[2]))
            self.mainui.tbllibrary.setItem(inx, 3, QTableWidgetItem(person[3]))
            self.mainui.tbllibrary.setItem(inx, 4, QTableWidgetItem(person[4]))
            self.mainui.tbllibrary.setItem(inx, 5, QTableWidgetItem(person[5]))
            self.mainui.tbllibrary.update()

    def search(self):
        column = ["Id", "Isbn", "Title", "Author", "Borrowed Time", "Borrower"]
        #self.mainui.tbllibrary.clear()
        self.mainui.tbllibrary.setColumnCount(len(column))
        self.mainui.tbllibrary.setHorizontalHeaderLabels(column)
    
        searched = sqlquerytitlesearch(self.mainui.editsearch.text())
        self.mainui.tbllibrary.setRowCount(1)
    
        try:
            if self.mainui.editsearch.text() != "":
                self.mainui.tbllibrary.setItem(0, 0, QTableWidgetItem(str(searched[0])))
                self.mainui.tbllibrary.setItem(0, 1, QTableWidgetItem(str(searched[1])))
                self.mainui.tbllibrary.setItem(0, 2, QTableWidgetItem(searched[2]))
                self.mainui.tbllibrary.setItem(0, 3, QTableWidgetItem(searched[3]))
                self.mainui.tbllibrary.setItem(0, 4, QTableWidgetItem(searched[4]))
                self.mainui.tbllibrary.setItem(0, 5, QTableWidgetItem(searched[5]))
                self.mainui.tbllibrary.update()
            else:
                self.loaddata()
        except:
            self.loaddata()


if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    main.show()
    app.exec()